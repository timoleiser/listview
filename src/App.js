import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Aufgabe_2 from "./Aufgabe_2";
import Aufgabe_3 from "./Aufgabe_3";
import Aufgabe_4 from "./Aufgabe_4";

function App() {
  return (
      <div>
          <h1>Aufgabe 1</h1>
          <b><p>Bootstrap im Projekt installieren</p></b>
              <p>Gemacht...</p>
          <h1>Aufgabe 2</h1>
          <b><p>Liste mit Bootstrap Komponenten umsetzen</p></b>
              <Aufgabe_2/>
          <h1>Aufgabe 3</h1>
          <b><p>Zwei Listen mit List-Group nebeneinander</p></b>
              <Aufgabe_3/>
          <h1>Aufgabe 4</h1>
          <b><p>Operationen zwischen zwei Listen</p></b>
              <Aufgabe_4/>
          <h1>Aufgabe 5</h1>
          <b><p>für Fortgeschrittene</p></b>
          <h1>Aufgabe 6</h1>
          <b><p>Eintrag aus der Liste links löschen</p></b>
          <h1>Aufgabe 7</h1>
          <b><p>Strukturierte Daten - Für Fortgeschrittene</p></b>
      </div>
  );
}

export default App;
