import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {ListGroup, Container, Row, Col} from "react-bootstrap";
import React, {useState} from "react";
function Aufgabe_3() {

    const data = [
        { title: 'Honda CBR 1000'},
        { title: 'Ducati Monster 120s'},
        { title: 'Yamaha MT-01'},
        { title: 'KTM Superduke 1200'},
        { title: 'Triumph Triple Speed'},
    ]
    const data2 = [
        { title: 'Suzuki Hayabusa'},
        { title: 'Moto Guzzi California'},
        { title: 'BMW C1'},
        { title: 'Honda CB 900'},
        { title: 'Yamaha BT 1100 Bulldog'},
    ]


    const [motorraeder, setMotorraeder] = useState(data);

    const [motorraeder2, setMotorraeder2] = useState(data2);

    const [angeklickt, setAngeklickt] = useState("");

    const [angeklickt2, setAngeklickt2] = useState("");

    return (
        <div className={"ListGroup"}>
            <Container>
                <Row>
                    <Col>
                        <ListGroup>
                            { motorraeder.map(motorrad => <ListGroup.Item action active={angeklickt===motorrad.title} onClick={()=>setAngeklickt(motorrad.title)}>{motorrad.title}</ListGroup.Item>)}
                        </ListGroup>
                    </Col>
                    <Col>
                        <ListGroup>
                            { motorraeder2.map(motorrad2 => <ListGroup.Item action active={angeklickt2===motorrad2.title} onClick={()=>setAngeklickt2(motorrad2.title)}>{motorrad2.title}</ListGroup.Item>)}
                        </ListGroup>
                    </Col>
                </Row>
            </Container>

        </div>
    );
}

export default Aufgabe_3;
