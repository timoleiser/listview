import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {ListGroup} from "react-bootstrap";
import React, {useState} from "react";
function Aufgabe_2() {

    const data = [
        { title: 'Honda CBR 1000'},
        { title: 'Ducati Monster 120s'},
        { title: 'Yamaha MT-01'},
        { title: 'KTM Superduke 1200'},
        { title: 'Triumph Triple Speed'},
    ]

    const [motorraeder, setMotorraeder] = useState(data);

    const [angeklickt, setAngeklickt] = useState("");

    return (
        <div className={"ListGroup"}>
            <ListGroup>
                { motorraeder.map(motorrad => <ListGroup.Item action active={angeklickt===motorrad.title} onClick={()=>setAngeklickt(motorrad.title)}>{motorrad.title}</ListGroup.Item>)}
            </ListGroup>
        </div>
    );
}

export default Aufgabe_2;
