import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {ListGroup, Container, Row, Col} from "react-bootstrap";
import React, {useState} from "react";
function Aufgabe_4() {

    const data = [
        { title: 'Honda CBR 1000'},
        { title: 'Ducati Monster 120s'},
        { title: 'Yamaha MT-01'},
        { title: 'KTM Superduke 1200'},
        { title: 'Triumph Triple Speed'},
    ]
    const data2 = [
        { title: 'Suzuki Hayabusa'},
        { title: 'Moto Guzzi California'},
        { title: 'BMW C1'},
        { title: 'Honda CB 900'},
        { title: 'Yamaha BT 1100 Bulldog'},
    ]


    const [motorraeder, setMotorraeder] = useState(data);

    const [motorraeder2, setMotorraeder2] = useState(data2);

    const [angeklickt, setAngeklickt] = useState(-1);

    const [angeklickt2, setAngeklickt2] = useState(-1);

    function oneLeftToRight(){
        setMotorraeder2([...motorraeder2, motorraeder[angeklickt]])
        let removedItem = [...motorraeder]
        removedItem.splice(angeklickt, 1);
        setMotorraeder(removedItem)
    }
    function oneRightToLeft(){
        setMotorraeder([...motorraeder, motorraeder2[angeklickt2]])
        let removedItem = [...motorraeder2]
        removedItem.splice(angeklickt2, 1);
        setMotorraeder2(removedItem)
    }
    function allToRight(){
        setMotorraeder2([...motorraeder2, ...motorraeder])
        setMotorraeder([])
    }
    function allToLeft(){
        setMotorraeder([...motorraeder, ...motorraeder2])
        setMotorraeder2([])
    }
    return (
        <div className={"ListGroup"}>
            <Container>
                <Row>
                    <Col>
                        <ListGroup>
                            {
                                motorraeder.map((motorrad,index) => <ListGroup.Item action active={
                                    angeklickt===index
                                } onClick={
                                    ()=>setAngeklickt(index)
                                }>{
                                    motorrad.title
                                }</ListGroup.Item>)
                            }
                        </ListGroup>
                    </Col>
                    <Col>
                        <Row><button className={"button"} onClick={()=>allToRight()}><b>&gt;&gt;</b></button></Row>
                        <Row><button className={"button"} onClick={()=>oneLeftToRight()}><b>&gt;</b></button></Row>
                        <Row><button className={"button"} onClick={()=>oneRightToLeft()}><b>&lt;</b></button></Row>
                        <Row><button className={"button"} onClick={()=>allToLeft()}><b>&lt;&lt;</b></button></Row>
                    </Col>
                    <Col>
                        <ListGroup>
                            {
                                motorraeder2.map((motorrad2,index2) => <ListGroup.Item action active={
                                    angeklickt2===index2
                                } onClick={
                                    ()=>setAngeklickt2(index2)
                                }>{
                                    motorrad2.title
                                }</ListGroup.Item>)
                            }
                        </ListGroup>
                    </Col>
                </Row>
            </Container>

        </div>
    );
}

export default Aufgabe_4;
